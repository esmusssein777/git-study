初始化git
```
git clone http://www.kernel.org/pub/scm/git/git.git

cd project

git init
```
Git会输出:
```
Initialized empty Git repository in .git/
```
如果你仔细观查会发现project目录下会有一个名叫”.git” 的目录被创建，这意味着一个仓库被初始化了。

### 常用的命令

#### 创建一个新的项目后

使用 

`git add .` 

`git commit -m ''`

`git remote add origin ...`

`git push -u origin master`

#### 使用分支

`git chenkout -b dev`创建 dev 分支

`git push origin dev` 创建远程的 dev 分支

#### 克隆他人的项目使用分支

`git clone ... `

`git checkout -b dev origin/dev`

`git branch` 可查看

`git pull origin dev` 拉到本地的 dev 分支

#### stash 和 pop

```
Git stash
git stash pop
```





强制使用git最新的代码覆盖本地

``git fetch --all && git reset --hard origin/master && git pull``

Git clone 慢

`git config --global http.postBuffer 524288000`

```
$ git clone http://github.com/large-repository --depth 1

$ cd large-repository

$ git fetch --unshallow
```